import math
import random
import time

class Problem:

    def __init__(self, fn, num1, num2, operator):
        self.fn = fn
        self.num1 = num1
        self.num2 = num2
        self.operator = operator

    def __str__(self):
        return str(self.num1) + " " + self.operator + " " + str(self.num2)

    def apply(self, input1, input2):
        return self.fn(input1, input2)

    def check_answer(self):
        return self.apply(self.num1, self.num2)

class Exercise:

    def __init__(self, fn, first_num_bounds, second_num_bounds, operator_desc):
        self.fn = fn
        self.first_num_bounds = first_num_bounds
        self.second_num_bounds = second_num_bounds
        self.operator_desc = operator_desc

    def gen_problem(self):
        num1 = random.randint(self.first_num_bounds[0], self.first_num_bounds[1])
        num2 = random.randint(self.second_num_bounds[0], self.second_num_bounds[1])

        return Problem(self.fn, num1, num2, self.operator_desc)

def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def square(x, y):
    return x ** 2

print("Choose a skill to practice: ")
print("A. 2 digit addition")
print("B. 3 digit addition")
print("C. 2 digit subtraction")
print("D. 3 digit subtraction")
print("E. 2x1 digit multiplication")
print("F. 3x1 digit multiplication")
print("G. 2 digit squares")

choice = input()
print("")

num_problems = 20
correct_counter = 0

exercise = ""

if choice == "A":
    print("You have selected 2 digit addition!")
    exercise = Exercise(add, [10,99], [10,99], "+")
elif choice == "B":
    print("You have selected 3 digit addition!")
    exercise = Exercise(add, [100,999], [100,999], "+")
elif choice == "C":
    print("You have selected 2 digit subtraction!")
    exercise = Exercise(subtract, [10,99], [10, 99], "-")
elif choice == "D":
    print("You have selected 3 digit subtraction!")
    exercise = Exercise(subtract, [100,999], [100,999], "-")
else:
    print("Either you entered something fake or it's not supported yet so you get squares")
    exercise = Exercise(square, [10,99], [2,2], "**")

input("Hit enter to start")
print("")

start_time = time.time()

for i in range(0, num_problems):
    problem = exercise.gen_problem()
    num = int(input("Problem #" + str(i) + ": " + str(problem) + " = ",))
    if num == problem.check_answer():
        print("You got it right!")
        correct_counter += 1
    else:
        print("WRONG! The correct answer was: " + str(problem.check_answer()))
    print("")

end_time = time.time()

print("Out of " + str(num_problems) + " questions, you got " + str(correct_counter) + " correct!")
print("This exercise took you a total of " + str(int(end_time - start_time)) + " seconds.")

